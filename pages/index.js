import {
  Tabs,
  Badge,
  Avatar,
  Button,
  Tag,
  Modal,
  Input,
  InputNumber,
  Popconfirm,
  DatePicker,
  Statistic,
  Select,
  Steps,
  Divider,
} from "antd";
import { FaFilter, FaMinus, FaPlus, FaSearch, FaUserAlt } from "react-icons/fa";
import { BiKey, BiUser } from "react-icons/bi";
import { useState } from "react";
import Image from "next/image";

const { Option } = Select;
const { Step } = Steps;
const { RangePicker } = DatePicker;

export default function Dashboard() {
  const [loggedIn, setLoggedIn] = useState(false);
  const [username, setUsername] = useState(null);
  const [password, setPassword] = useState(null);
  const [password2, setPassword2] = useState(null);
  const [modalVisible, setModalVisible] = useState(false);
  const [phoneNumber, setPhoneNumber] = useState(null);
  const [name, setName] = useState(null);

  const handleLogOut = () => {
    setLoggedIn(false);
  };

  const Login = () => {
    const [load, setLoad] = useState(true);
    const handleLogin = () => {
      setLoggedIn(true);
    };
    const handleSignup = () => {
      setLoggedIn(true);
    };

    if (load) return <p className="font-bold">Loaded</p>;

    return (
      <div className="h-screen w-screen bg-blue-500 relative">
        <div className="p-4 absolute top-[30%] left-[50%] translate-x-[-50%] rounded-md bg-white sm:max-w-[70%] md:max-w-[40%]">
          <h2 className="text-xl text-center tracking-tighter uppercase">
            Suppliers Portal
          </h2>
          <Divider />
          <Input
            placeholder="Username / Phone number"
            prefix={<BiUser className="text-gray-500" />}
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
          <Input.Password
            className="my-3"
            placeholder="Password"
            prefix={<BiKey className="text-gray-500" />}
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
          <Button type="primary" block onClick={handleLogin}>
            Continue
          </Button>
          <p className="text-center m-0 mt-3">
            Not a supplier yet?{" "}
            <Button
              type="link"
              style={{ display: "inline" }}
              onClick={() => setModalVisible(true)}
            >
              Become a supplier
            </Button>
          </p>
          <Modal
            open={modalVisible}
            title="Be a supplier!"
            onOk={handleSignup}
            onCancel={() => setModalVisible(false)}
          >
            <p>
              We noticed you aren&apos;t a supplier just yet. Fill your business
              details down below to get you started{" "}
            </p>
            <Input
              value={name}
              onChange={(e) => setName(e.target.value)}
              placeholder="Name ex. Kamwangi suppliers"
            />
            <Input
              value={phoneNumber}
              onChange={(e) => setPhoneNumber(e.target.value)}
              placeholder="Phone number ex. 0748920306"
              style={{ marginTop: "12px" }}
            />
            <Input.Password
              className="my-3"
              placeholder="Password"
              prefix={<BiKey className="text-gray-500" />}
              value={password2}
              onChange={(e) => setPassword2(e.target.value)}
            />
          </Modal>
        </div>
      </div>
    );
  };

  const Sales = () => {
    return null;
  };

  const Product = (props) => {
    const [restockModal, setRestockModal] = useState(false);
    const [editModal, setEditModal] = useState(false);
    const [newName, setNewName] = useState(null);
    const [newPrice, setNewPrice] = useState(null);

    const handleEdit = () => {};
    const handleRemove = () => {};
    const incrementStock = () => {};
    const decrementStock = () => {};

    const handleCancel = () => {
      setEditModal(false);
      setRestockModal(false);
      setNewPrice(null);
      setNewName(null);
    };

    return (
      <div className="bg-gray-50 col-span-1 p-3">
        <Image width={100} height={100} src={"/favicon.ico"} />
        <p className="font-bold tracking-tight m-0 mt-2">Product name</p>
        <p className="m-0">Ksh. 200</p>
        <Tag color="orange">23 left</Tag>
        <div className="flex -space-x-2 mt-2">
          <Button type="link" onClick={() => setRestockModal(true)}>
            Restock
          </Button>
          <Button type="link" onClick={() => setEditModal(true)}>
            Edit
          </Button>
          <Popconfirm
            title="Remove product ?"
            okText="Yes"
            cancelText="No"
            onConfirm={handleRemove}
          >
            <Button type="link">Remove</Button>
          </Popconfirm>
        </div>

        <Modal
          visible={restockModal}
          title="Restock"
          footer={null}
          onCancel={handleCancel}
        >
          <p>Update quantity of items:</p>
          <div className="flex space-x-4">
            <Button type="primary" onClick={decrementStock}>
              <FaMinus />
            </Button>
            <h2 className="font-bold">{12}</h2>
            <Button type="primary" onClick={incrementStock}>
              <FaPlus />
            </Button>
          </div>
        </Modal>
        <Modal
          visible={editModal}
          title="Edit"
          onOk={handleEdit}
          onCancel={handleCancel}
        >
          <p>Update product information</p>
          <Input
            value={newName}
            allowClear
            onChange={(e) => setNewName(e.target.value)}
            placeholder="New product name"
          />
          <div className="block w-full my-2">
            <InputNumber
              prefix="Ksh."
              value={newPrice}
              style={{ width: "100%" }}
              allowClear
              onChange={(val) => setNewPrice(val)}
              placeholder="New product price"
            />
          </div>
        </Modal>
      </div>
    );
    ``;
  };

  const ProductList = () => {
    const [addModal, setAddModal] = useState(false);
    const [name, setName] = useState(null);
    const [price, setPrice] = useState(null);
    const [quantity, setQuantity] = useState(null);

    const handleCloseAdd = () => {
      setAddModal(false);
    };

    const handleAddProduct = () => {
      null;
    };

    return (
      <div className="relative">
        <div className="min-h-[calc(100vh-150px)] max-h-[calc(100vh-150px)] overflow-auto grid grid-cols-2 gap-3">
          {[1, 2, 3, 4, 5].map((el, i) => (
            <Product key={i} />
          ))}
        </div>
        <button
          onClick={() => setAddModal(true)}
          className="bg-[#f56a00]-900 p-4 rounded-[50%] border border-gray-300 absolute right-4 bottom-4 "
        >
          <FaPlus />
        </button>
        <Modal
          visible={addModal}
          title="Add product"
          onCancel={handleCloseAdd}
          onOk={handleAddProduct}
        >
          <Input
            value={name}
            required
            onChange={(e) => setName(e.target.value)}
            placeholder="Product name"
          />
          <div className="block w-full my-2">
            <InputNumber
              prefix="Ksh."
              value={price}
              style={{ width: "100%" }}
              required
              onChange={(val) => setPrice(val)}
              placeholder="Price"
            />
          </div>
          <InputNumber
            suffix="Items"
            value={quantity}
            style={{ width: "100%" }}
            required
            onChange={(val) => setQuantity(val)}
            placeholder="Quantity"
          />
        </Modal>
      </div>
    );
  };

  const Analytics = () => {
    return null;
  };

  const items = [
    {
      label: "Sales",
      key: "item-0",
      children: <Sales />,
    },
    { label: "Products", key: "item-1", children: <ProductList /> },
    { label: "Analytics", key: "item-2", children: <Analytics /> },
  ];

  if (!loggedIn) return <Login />;

  return (
    <div>
      {/* Header */}
      <div className="flex justify-between px-2 py-3">
        <p className="font-bold tracking-tight text-gray-900 text-2xl">
          Dashboard
        </p>
        <div className="flex space-x-3">
          <Popconfirm title="Log out" onConfirm={handleLogOut}>
            <Avatar style={{ color: "#f56a00", backgroundColor: "#fde3cf" }}>
              U
            </Avatar>
          </Popconfirm>
        </div>
      </div>

      {/* Tabs */}
      <div className="px-3">
        <Tabs items={items} />
      </div>
    </div>
  );
}
